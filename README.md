# appxuptime.go

_A Go port of [appx_uptime.py](https://gitlab.com/gmobrien/appx-uptime.py)_

This is a simple tool I use to generate a cute uptime message in plain
English.

It probably has no practical value.

## Build

To build **appxutime**, check out the project and run `go build appxuptime` within the project directory. 

## Usage

`./appxuptime`

This will produce output on STDOUT similar to:

`This system has been online for approximately 20 hours and 10 minutes.`

Enjoy!
