// Copyright (c) 2019, Gabriel O'Brien. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// appxuptime is a simple tool to print the approximate system uptime in
//plain English. Ported to Go from https://gitlab.com/gmobrien/appx-uptime.py
package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

// check verifies the output of functions that return values, panic on error.
func check(err error) {
	if err != nil {
		panic(err)
	}
}

// getUptime formats the data in /proc/uptime and returns an int value in
// seconds.
// format: <uptime in seconds> <total cpu idle in seconds>
func getUptime() (upfloat float64) {
	data, err := ioutil.ReadFile("/proc/uptime")
	check(err)

	// create array from /proc/uptime input
	fields := strings.Fields(string(data))

	// input is a float, convert to int
	upfloat, err = strconv.ParseFloat(fields[0], 64)
	check(err)

	return
}

// convertDayHrMin converts the uptime data in seconds into units that are
// more natural for meat popsicles, also return useful modulus values.
func convertDayHrMin(seconds float64) (dd, hh, mm, hhMod, mmMod int) {
	dd = int(seconds / 86400)
	hh = int(seconds / 3600)
	mm = int(seconds / 60)
	hhMod = int(hh % 24)
	mmMod = int(mm % 60)
	return
}

// printUptime formats the output and prints to STDOUT.
func printUptime(dd, hh, mm, hhMod, mmMod int) {

	pre := "This system has been online for approximately"
	ddStr := "days"
	hhStr := "hours"

	// format unit strings to simplify if statements
	if dd == 1 {
		ddStr = "day"
	}

	if hhMod == 1 {
		hhStr = "hour"
	}

	// round down via int arithmetic
	mmRound := (mmMod / 10) * 10

	days := fmt.Sprintf("%d %s", dd, ddStr)
	hours := fmt.Sprintf("%d %s", hhMod, hhStr)
	minutes := fmt.Sprintf("%d %s", mmRound, "minutes")

	// walk through various English grammar cases and print properly formatted output.
	if dd == 0 {
		if hhMod == 0 {
			if mmRound == 0 {
				fmt.Println("This system has just come online.")
			} else {
				fmt.Printf("%s %s.\n", pre, minutes)
			}
		} else {
			if mmRound == 0 {
				fmt.Printf("%s %s.\n", pre, hours)
			} else {
				fmt.Printf("%s %s and %s.\n", pre, hours, minutes)
			}
		}
	} else {
		if hhMod == 0 {
			if mmRound == 0 {
				fmt.Printf("%s %s.\n", pre, days)
			} else {
				fmt.Printf("%s %s and %s.\n", pre, days, minutes)
			}
		} else {
			if mmRound == 0 {
				fmt.Printf("%s %s and %s.\n", pre, days, hours)
			} else {
				fmt.Printf("%s %s, %s, and %s.\n", pre, days, hours, minutes)
			}
		}
	}
}

func main() {
	printUptime(convertDayHrMin(getUptime()))
}
